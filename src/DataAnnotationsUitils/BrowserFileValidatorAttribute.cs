﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Components.Forms;

namespace DataAnnotationsUitils
{
    [AttributeUsage(AttributeTargets.Property)]
    public class BrowserFileValidatorAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            if (value is null)
            {
                ErrorMessage = "File is required";
                return false;
            }

            if (value is not IBrowserFile browserFile)
            {
                ErrorMessage = $"Type of file property has wrong type {value.GetType().Name} and correct type is {typeof(IBrowserFile)}";
                return false;
            }

            if (!browserFile.ContentType.Contains("image"))
            {
                ErrorMessage = $"File must be an image but the current type of file is {browserFile.ContentType}";
                return false;
            }

            return true;
        }
    }
}