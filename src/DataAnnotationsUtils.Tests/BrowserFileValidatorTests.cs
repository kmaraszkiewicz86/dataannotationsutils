using DataAnnotationsUitils;
using FluentAssertions;
using Microsoft.AspNetCore.Components.Forms;
using Moq;

namespace DataAnnotationsUtils.Tests
{
    public class BrowserFileValidatorTests
    {
        private BrowserFileValidatorAttribute _browserFileValidator;

        public BrowserFileValidatorTests()
        {
            _browserFileValidator = new BrowserFileValidatorAttribute();
        }

        [Fact]
        public void When_FileIsNull_ThenReturnFalse()
        {
            IBrowserFile browserFile = null;

            var result = _browserFileValidator.IsValid(browserFile);

            result.Should().BeFalse();
            _browserFileValidator.ErrorMessage.Should().Be("File is required");
        }

        [Fact]
        public void When_FileIsntImage_ThenReturnFalse()
        {
            Mock<IBrowserFile> browserFile = new Mock<IBrowserFile>();
            browserFile.SetupGet(mock => mock.ContentType).Returns("text/plain");

            var result = _browserFileValidator.IsValid(browserFile.Object);

            result.Should().BeFalse();
            _browserFileValidator.ErrorMessage.Should().Be("File must be an image but the current type of file is text/plain");
        }

        [Fact]
        public void When_FileIsntIBrowserFileType_ThenReturnFalse()
        {
            var browserFile = new object();

            var result = _browserFileValidator.IsValid(browserFile);

            result.Should().BeFalse();
            _browserFileValidator.ErrorMessage.Should().Be($"Type of file property has wrong type Object and correct type is {typeof(IBrowserFile)}");
        }

        [Fact]
        public void When_FileIsImage_ThenReturnTrue()
        {
            Mock<IBrowserFile> browserFile = new Mock<IBrowserFile>();
            browserFile.SetupGet(mock => mock.ContentType).Returns("image/jpeg");

            var result = _browserFileValidator.IsValid(browserFile.Object);

            result.Should().BeTrue();
            _browserFileValidator.ErrorMessage.Should().BeNullOrEmpty();
        }
    }
}